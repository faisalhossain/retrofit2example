package com.shadowhite.retrofit2example.ui.main;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.shadowhite.retrofit2example.R;
import com.shadowhite.retrofit2example.util.remote.RemoteApiInterface;
import com.shadowhite.retrofit2example.util.remote.RemoteApiProvider;
import com.shadowhite.retrofit2example.util.remote.model.LoginModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
private RemoteApiInterface mService;
private String TAG=getClass().getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mService=RemoteApiProvider.getInstance().getRemoteApi();

        mService.login("rezoyanulislam890@gmail.com","password").enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                if(response.isSuccessful())
                {
                    if(response.body()!=null){
                        LoginModel model=response.body();
                        Log.d(TAG,"response:"+model.toString());
                    }
                    else {
                        Log.d(TAG,"response is null");
                    }

                }
                else {
                    Log.d(TAG,"error code: "+response.code());
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                Log.d(TAG,"failed: "+t.getMessage());
            }
        });

    }
}

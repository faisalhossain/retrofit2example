package com.shadowhite.retrofit2example.util.remote;

import com.shadowhite.retrofit2example.util.remote.model.LoginModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface RemoteApiInterface {
    /**
     * Request to server for registration
     * */

    @FormUrlEncoded
    @POST("login")
    Call<LoginModel> login(@Field("email") String email, @Field("password") String password);


}
